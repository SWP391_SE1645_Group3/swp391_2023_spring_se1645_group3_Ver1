<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : tourCate
    Created on : 14-02-2023, 10:14:28
    Author     : pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Free Bootstrap Themes by Zerotheme dot com - Free Responsive Html5 Templates">
        <meta name="author" content="https://www.Zerotheme.com">

        <title>Fashion Shop | Free Bootstrap Themes by Zerotheme.com</title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="../css/bootstrap.min.css"  type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/style.css">


        <!-- Custom Fonts -->
        <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css"  type="text/css">
        <link rel="stylesheet" href="../fonts/font-slider.css" type="text/css">

        <!-- jQuery and Modernizr-->
        <script src="../js/jquery-2.1.1.js"></script>

        <!-- Core JavaScript Files -->  	 
        <script src="../js/bootstrap.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!--Top-->
        <nav id="top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="top-link">
                            <li><a href="signup"><span class="glyphicon glyphicon-user"></span> My Account</a>
                                <ul class="sub-my-account">
                                    <li><a href="#"> My profile</a></li>
                                    <li><a href="#">Edit Profile</li>
                                    <li>View My Tour</li>
                                    <li>My Point</li>
                                    <li>Log Out</li>
                                </ul>
                            </li>
                            <li><a href="contact.html"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!--Header-->
        <header >
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="logo"><img src="../image/logo.png" /></div>
                    </div>
                    <div class="col-md-6 text-right">
                        <form class="form-search" action="searchtour">  
                            <input type="text" class="input-medium search-query">  
                            <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>  
                        </form>
                    </div>
                    <div id="cart"><a class="btn btn-cart" href="cart.html"><span class="glyphicon glyphicon-shopping-cart"></span>CART<strong>0</strong></a></div>
                </div>
            </div>
        </header>
        <!--Navigation-->
        <nav id="menu" class="navbar">
            <div class="container">
                <div class="navbar-header"><span id="heading" class="visible-xs">Categories</span>
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Home</a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Category</a>
                            <div class="dropdown-menu">
                                <div class="dropdown-inner">
                                    <ul class="list-unstyled">
                                        <c:forEach var="o" items="${lstTourCate}">
                                        
                                        <li><a href="listtourbycategory?categoryid=${o.tour_category_id}">${o.tour_category_name}</a></li>
                                       
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Service(Nhi)</a>
                            <div class="dropdown-menu">
                                <div class="dropdown-inner">
                                    <ul class="list-unstyled">
                                        <li><a href="category.html">Text 201</a></li>
                                        <li><a href="category.html">Text 202</a></li>
                                        <li><a href="category.html">Text 203</a></li>
                                        <li><a href="category.html">Text 204</a></li>
                                        <li><a href="category.html">Text 205</a></li>
                                    </ul>
                                </div> 
                            </div>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Booking(Kien)</a>
                            <div class="dropdown-menu" style="margin-left: -203.625px;">
                                <div class="dropdown-inner">
                                    <ul class="list-unstyled">
                                        <li><a href="category.html">Create Tour</a></li>
                                        <li><a href="category.html">Edit Tour</a></li>
                                        <li><a href="category.html">Delete Tour</a></li>
                                        <li><a href="category.html">Text 304</a></li>
                                        <li><a href="category.html">Text 305</a></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><a href="category.html">Create Voucher</a></li>
                                        <li><a href="category.html">Edit Voucher</a></li>
                                        <li><a href="category.html">Delete Voucher</a></li>
                                        <li><a href="category.html">Text 309</a></li>
                                        <li><a href="category.html">Text 310</a></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><a href="category.html">User List</a></li>
                                        <li><a href="category.html">Text 312</a></li>
                                        <li><a href="category.html#">Text 313</a></li>
                                        <li><a href="category.html#">Text 314</a></li>
                                        <li><a href="category.html">Text 315</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li><a href="introduce?user_id=${sessionScope.post.user_id}">Introduce(Thanh)</a></li>
                        <li><a href="category.html">Hot Fashion</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <!--//////////////////////////////////////////////////-->
        <!--///////////////////HomePage///////////////////////-->
        <!--//////////////////////////////////////////////////-->
        <div id="page-content" class="home-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Carousel -->
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators hidden-xs">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img style="width: 100%" src="../image/canh-dep-ha-noi-viet-nam_055419712.jpg" alt="First slide">
                                    <!-- Static Header -->
                                    <div class="header-text hidden-xs">
                                        <div class="col-md-12 text-center">
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                                <div class="item">
                                    <img style="width: 100%" src="../image/canh-dep-pho-co-hoi-an_055419868.jpg" alt="Second slide">
                                    <!-- Static Header -->
                                    <div class="header-text hidden-xs">
                                        <div class="col-md-12 text-center">
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                                <div class="item">
                                    <img style="width: 100%" src="../image/canh-dep-hang-dong-viet-nam_055419790.jpg" alt="Third slide">
                                    <!-- Static Header -->
                                    <div class="header-text hidden-xs">
                                        <div class="col-md-12 text-center">
                                        </div>
                                    </div><!-- /header-text -->
                                </div>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div><!-- /carousel -->
                    </div>
                </div>
                <div class="row">
                    <div class="banner">
                        <div class="col-sm-4">
                            <img style="height: 100%" src="../image/image.png" />
                        </div>
                        <div class="col-sm-4">
                            <img style="height: 100%" src="../image/canh-dep-dong-muoi-viet-nam_055419649.jpg" />
                        </div>
                        <div class="col-sm-4">
                            <img src="../image/anh-dep-nguoi-dan-vung-cao-viet-nam_055419258.jpg" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#featured">FEATURED PRODUCTS</a></li>
                        <li><a href="#new">NEWS</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="featured" class="tab-pane fade in active">
                            <div class="products">
                                <div class="col-sm-7 four-three">
                                    <div class="row">
                                        <c:forEach var="o" items="${lst}">
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="tourdetail?tourid=${o.tour_id}"><img style="height: 105px; width: 100%" src="../${o.tour_image}" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">${o.tour_name}</a></h3></div>
                                                    <div class="">${o.capacity}</div>
                                                    <div class="price">${o.tour_price}VND</div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        </c:forEach>
                                        
                                    </div><!-- end inner row -->
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div id="new" class="tab-pane fade">
                            <div class="products">
                                <div class="col-sm-7 five-three">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img style="height: 105px; width: 100%" src="../image/canh-dep-dao-viet-nam-nhin-tu-tren-cao_055419602.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">1.000.000đ</div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img style="height: 105px; width: 100%" src="../image/canh-dep-dong-muoi-viet-nam_055419649.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img style="height: 105px; width: 100%" src="../image/canh-bien-dep-viet-nam_055419524.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div><!-- end inner row -->
                                    </div>
                                </div>
                                <div class="col-sm-5 five-two">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img style="height: 105px; width: 100%" src="../image/anh-dep-nui-deo-viet-nam_055419305.jpg" /></a>
                                                    <div class="hot">
                                                        <span>HOT</span>
                                                    </div>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">	
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img style="height: 105px; width: 100%" src="../image/anh-phong-canh-dep-o-viet-nam_055419462.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>

                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end inner row -->
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="banner">
                        <div class="col-sm-6">
                            <img style="height: 253.55px; width: 455px" src="../image/anh-phong-canh-dep-o-viet-nam_055419462.jpg" />
                        </div>
                        <div class="col-sm-6">
                            <img style="height: 253.55px; width: 455px " src="../image/anh-phong-canh-viet-nam-dep_055419477.jpg" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#best">BEST SALES</a></li>
                        <li><a href="#special">SPECIAL</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="best" class="tab-pane fade in active">
                            <div class="products">
                                <div class="col-sm-7 five-three">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp3_2.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp5_1.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp6_1.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div><!-- end inner row -->
                                    </div>
                                </div>
                                <div class="col-sm-5 five-two">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp12_1.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp19_1.jpg" /></a>
                                                    <div class="hot">
                                                        <span>HOT</span>
                                                    </div>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end inner row -->
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div id="special" class="tab-pane fade">
                            <div class="products">
                                <div class="col-sm-7 five-three">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp19_1.jpg" /></a>
                                                    <div class="hot">
                                                        <span>HOT</span>
                                                    </div>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp5_1.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp12_1.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                            </div>
                                        </div><!-- end inner row -->
                                    </div>
                                </div>
                                <div class="col-sm-5 five-two">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp6_1.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">

                                            <div class="product">
                                                <div class="image">
                                                    <a href="product.html"><img src="images/clothing_sp3_2.jpg" /></a>
                                                    <ul class="buttons">
                                                        <li><a class="btn btn-2 cart" href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                                                        <li><a class="btn btn-2 wishlist" href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
                                                        <li><a class="btn btn-2 compare" href="#"><span class="glyphicon glyphicon-transfer"></span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="caption">
                                                    <div class="name"><h3><a href="product.html">Pretty Playsuit</a></h3></div>
                                                    <div class="price">$122<span>$98</span></div>
                                                    <div class="rating"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end inner row -->
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="brand">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <a href="#"><img src="../image/brand1-250x100.jpg" /></a>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <a href="#"><img src="../image/brand2-250x100.jpg" /></a>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <a href="#"><img src="../image/brand4-250x100.jpg" /></a>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <a href="#"><img src="../image/brand1-250x100.jpg" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <h4>Subcribe Email</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                        <div class="col-md-6">
                            <form name="subcribe-email" action="subcribe.php">
                                <div class="subcribe-form form-group">
                                    <input class="form-inline" type="text" name="email" value="1"><button href="#" class="btn btn-4" type="submit">Subcribe</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="container">
                <div class="wrap-footer">
                    <div class="row">
                        <div class="col-md-3 col-footer footer-1">
                            <img src="../image/logo.png" />
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                        <div class="col-md-3 col-footer footer-2">
                            <div class="heading"><h4>Customer Services</h4></div>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Delivery Information</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-footer footer-3">
                            <div class="heading"><h4>My Account</h4></div>
                            <ul>
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">Brands</a></li>
                                <li><a href="#">Gift Vouchers</a></li>
                                <li><a href="#">Specials</a></li>
                                <li><a href="#">Site Map</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-footer footer-4">
                            <div class="heading"><h4>Contact Us</h4></div>
                            <ul>
                                <li><span class="glyphicon glyphicon-home"></span>California, United States 3000009</li>
                                <li><span class="glyphicon glyphicon-earphone"></span>+91 8866888111</li>
                                <li><span class="glyphicon glyphicon-envelope"></span>infor@yoursite.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            Your Store ?? 20xx - Designed by <a href="https://www.Zerotheme.com" target="_blank">Zerotheme</a>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <ul>
                                    <li><img src="images/visa-curved-32px.png" /></li>
                                    <li><img src="images/paypal-curved-32px.png" /></li>
                                    <li><img src="images/discover-curved-32px.png" /></li>
                                    <li><img src="images/maestro-curved-32px.png" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- JS -->
        <script>
            $(document).ready(function () {
                $(".nav-tabs a").click(function () {
                    $(this).tab('show');
                });
                $('.nav-tabs a').on('shown.bs.tab', function (event) {
                    var x = $(event.target).text();         // active tab
                    var y = $(event.relatedTarget).text();  // previous tab
                    $(".act span").text(x);
                    $(".prev span").text(y);
                });
            });
        </script>

    </body>
</html>
