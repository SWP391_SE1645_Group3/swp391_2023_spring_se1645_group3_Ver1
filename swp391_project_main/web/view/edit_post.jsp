<%-- 
    Document   : edit_post
    Created on : Feb 26, 2023, 7:18:07 PM
    Author     : anhth
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="edit" method="post"> 
            <div class="row">
                <div class="col-md-9">
                    The New Post<input type="hidden" value="${post.user.user_id}">
                    <h4>Title</h4>
                    <input type="text"value="${post.title}" name="input_title">
                    <h4>Content</h4>
                    <input type="text" value="${post.content}" name="input_content">
                    <h4>Type</h4>
                    <input type="text" value="${post.type}" name="type">
                    <h4>Description</h4>
                    <input type="text" value="${post.description}" name="input_description">
                    <h4>User ID</h4>
                    <input type="text" value="${post.user.user_id}" name="user_id">
                    <h4>Image</h4>
                    <input type="file" value="${post.post_image}" name="input_photo">
                    <h4>Status</h4>
                    <input type="radio" name="status" value="On"> On
                    <input type="radio" name="status" value="Off"> Off
                </div>
                <div class="col-md-2">
                    <input type="submit" value="Update">
                </div>
            </div>
        </form>
    </body>
</html>
