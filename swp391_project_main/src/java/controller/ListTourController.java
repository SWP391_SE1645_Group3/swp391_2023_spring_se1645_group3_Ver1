/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.TourDBContext;
import entity.Tour;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author admin
 */
public class ListTourController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
//        try {
//        String indexPage = request.getParameter("index");
//        if(indexPage == null){
//            indexPage = "1";
//        }
//        int index = Integer.parseInt(indexPage);
//
//        //b1: getTotalTour
//        TourDBContext tourDB = new TourDBContext();
//        int count = tourDB.getTotalTour();
//        int endPage = count / 3;
//        if (count % 3 != 0) {
//            endPage++;
//        }
//            List<Tour> list = tourDB.pagingTour(index);
////           response.getWriter().println(list);
//            request.setAttribute("listA", list);
//            request.setAttribute("endP", endPage);
//           
//            request.getRequestDispatcher("/view/homepage.jsp").forward(request, response);
//        } catch (NumberFormatException e) {
//        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
//       int tour_id = Integer.parseInt(request.getParameter("id"));
//        TourDBContext tourDB = new TourDBContext();
//        Tour tour = tourDB.get(tour_id);
//        request.setAttribute("tour", tour);
//        request.getRequestDispatcher("view/category.jsp").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
