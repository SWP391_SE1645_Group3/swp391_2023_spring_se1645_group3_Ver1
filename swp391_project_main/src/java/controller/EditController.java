/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.PostDBContext;
import entity.Post;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author anhth
 */
public class EditController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int post_id = Integer.parseInt(req.getParameter("post_id"));
        PostDBContext post = new PostDBContext();
        Post p = post.get(post_id);
        req.setAttribute("post", p);
        req.getRequestDispatcher("edit_post.jsp").forward(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int post_id = Integer.parseInt(req.getParameter("post_id"));
        String title = req.getParameter("input_title");
        String content = req.getParameter("input_content");
        String type = req.getParameter("type");
        String description = req.getParameter("input_description");
        int user_id = Integer.parseInt(req.getParameter("user_id"));
        String photo = req.getParameter("input_photo");
        String status = req.getParameter("status");
        PostDBContext dao = new PostDBContext();
        Post p = new Post();
        dao.update(p);
        resp.sendRedirect("../introduce");
        
    }
    
}
